let g:syntastic_c_compiler = 'avr-gcc'
let g:syntastic_c_check_header = 1
let g:syntastic_c_compiler_options = '-mmcu=atmega16 -Os -Wall -Wstrict-prototypes -DF_OSC=1000000UL -DF_CPU=1000000UL'
